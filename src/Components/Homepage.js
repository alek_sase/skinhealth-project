import React from "react";
import HomeContent from "./HomeContent";
import Navbar from "./Navbar";

function Homepage(props) {
  return (
    <div>
      <Navbar />
      <HomeContent />
    </div>
  );
}

export default Homepage;
