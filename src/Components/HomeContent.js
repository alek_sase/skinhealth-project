import React, { useEffect, useState } from "react";
import { defaultItems } from "../db/data";
import { AppstoreOutlined, CreditCardOutlined } from "@ant-design/icons";
import OptionSelect from "./OptionSelect";
import ServiceFilter from "./ServiceFilter";

function HomeContent(props) {
  console.log(defaultItems);

  const [active, setActive] = useState("all");
  const [filterActive, setFilterActive] = useState([]);
  const [serviceFilter, setServiceFilter] = useState("clinic");
  const [filteredOptions, setFilteredOptions] = useState([]);

  useEffect(() => {
    showAllCategories();
  }, []);

  function handleFilteredOptions(current) {
    const arr = [];
    filterActive.map((x) => {
      if (current === x.name) {
        x.subCategory.map((y) => {
          arr.push({
            key: y.key,
            name: y.name,
            review: y.review,
            price: y.price,
            time: y.time,
            online: y.online,
            rating: y.rating,
          });
        });
      }
    });
    setFilteredOptions(arr);
  }

  function showAllCategories() {
    const arr = [];
    defaultItems.map((x) => {
      x.category.map((y) =>
        arr.push({
          key: y.key,
          name: y.name,
          value: y.rdmValue,
          subCategory: y.subCategory,
        })
      );
    });
    setFilterActive(arr);
  }

  return (
    <div className="homeContent">
      <div className="options">
        <div
          onClick={function () {
            setActive("all");
            showAllCategories();
          }}
          className={active === "all" ? "item activeItem" : "item"}
        >
          <AppstoreOutlined
            style={{ fontSize: "40px", color: "#54b2d3" }}
            theme="outlined"
          />
          <span>All</span>
        </div>
        {defaultItems.map((x) => (
          <div
            key={x.key}
            onClick={function () {
              setActive(x.name);
              setFilterActive(x.category);
              handleFilteredOptions(x.category[0].name);
            }}
            className={active === `${x.name}` ? "item activeItem" : "item"}
          >
            {console.log(x.category[0].name)}
            <img src={x.icon} alt="icon" />
            <span>{x.name}</span>
          </div>
        ))}
        <div className="item">
          <CreditCardOutlined
            style={{ fontSize: "40px", color: "rgba(255, 166, 0, 0.501)" }}
            theme="outlined"
          />
          <span>Voucher</span>
        </div>
      </div>

      <div className="secondRow">
        <OptionSelect
          data={filterActive}
          handleFilteredOptions={handleFilteredOptions}
        />
        <div className="optionDetails">
          <ServiceFilter
            serviceFilter={serviceFilter}
            setServiceFilter={setServiceFilter}
            data={filteredOptions}
          />
        </div>
      </div>
    </div>
  );
}

export default HomeContent;
